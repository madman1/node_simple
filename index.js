const express = require('express');
const app = new express();

app.get('/', function(request, response){
    //response.sendfile('index.html');

    var id = request.param('id');
    console.log(id);
    id = id == null ? 'M7lc1UVf-VE' : id;
    response.render(__dirname + "/index.html", {videoId:id});
});

app.engine('html', require('ejs').renderFile);
app.listen(8080);
console.log('listening on port 8080');